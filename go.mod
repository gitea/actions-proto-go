module code.gitea.io/actions-proto-go

go 1.19

require (
	connectrpc.com/connect v1.15.0
	google.golang.org/protobuf v1.32.0
)
